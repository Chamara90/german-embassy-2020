# LOLC Client

### Gulp setup

#### Plugins

* [node.js]
* [Gulp] 
* [Bower]

#### Installation

NPM package install

```sh
$ npm install
```

Bower component Install

```sh
$ bower install 
```

Run Gulp

```sh
$ gulp
```
