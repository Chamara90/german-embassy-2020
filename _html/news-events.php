<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>German Embassy - News and Events</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0 inner-page-main-topic">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="topic-wrap">
                        <h1>News and Events</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <main class="main-content homepage-main-content pt-0">

        <div class="container">
            <div class="breadcrumb-section">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">News and Events</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="container-fluid p-0 inner-page-secondary-topic">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="topic-wrap">
                            <h1>Actual News from Sri Lanka</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-1.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-2.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-3.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-4.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-5.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-6.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container" id ="news-content">
                <div class="row">
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-1.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-2.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-3.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container see-more-btn mt-3">
                <div class="row">
                    <div class="col-12 text-center">
                        <button class="btn" id="news-show-hide">Read More</button>
                    </div>
                </div>
            </div>           
        </div>

        <div class="container-fluid p-0 inner-page-secondary-topic mt-5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="topic-wrap">
                            <h1>Actual News of the Embassy</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-7.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-9.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-10.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-11.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-11.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-12.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container" id ="news-content-2">
                <div class="row">
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-7.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-9.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="ne-card">                      
                            <div class="ne-img">
                                <img alt="News image" class="img-fluid" src="assets/images/news-10.jpg">
                            </div>
                            <div class="ne-info">
                                <div class="d-flex justify-content-between">
                                    <span class="date">2020/08/25</span>
                                    <span class="article">Article</span>
                                </div>
                            </div>
                            <div class="ne-title">
                                <a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. <span>&#8594;</span></p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container see-more-btn mt-3">
                <div class="row">
                    <div class="col-12 text-center">
                        <button class="btn" id="news-show-hide-2">Read More</button>
                    </div>
                </div>
            </div>           
        </div>

        <div class="container-fluid p-0 inner-page-secondary-topic mt-5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="topic-wrap">
                            <h1>Events</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Embassy Events <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/home-news-1.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Trade Events <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/home-news-1.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                   
                            <a>                            
                                <h2>Tourism Events <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/home-news-1.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Events in Sri Lanka <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/home-news-1.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Multimedia <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/home-news-1.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        


    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

    <script>
        $(document).ready(function () {

            var btn = $('#news-show-hide');
            $('#news-content').hide();
            $(btn).on('click', function() {
                $('#news-content').slideToggle(500);
                $(this).text(function(i, v){
                    return v === 'See Less' ? 'See More' : 'See Less'
                 })
            });

            var btn = $('#news-show-hide-2');
            $('#news-content-2').hide();
            $(btn).on('click', function() {
                $('#news-content-2').slideToggle(500);
                $(this).text(function(i, v){
                    return v === 'See Less' ? 'See More' : 'See Less'
                 })
            });

        });
    </script>

</body>
</html>
