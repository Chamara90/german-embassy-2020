<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>German Embassy - Tourism</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0 inner-page-main-topic">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="topic-wrap">
                        <h1>Tourism</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <main class="main-content homepage-main-content pt-0">

        <div class="container">
            <div class="breadcrumb-section">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tourism</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="container-fluid p-0 inner-page-secondary-topic">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="topic-wrap">
                            <h1>10 good Reasons to visit Sri Lanka</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section mb-4">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="accordion" class="panel-body">
                            <div class="panel">
                                <div class="panel-header" id="headingOne">
                                    <p class="panel-topic mb-0">
                                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Diversity
                                        </button>
                                    </p>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body panel-description">
                                        <p>With many sites and scenes bottled up in to a small island, a traveller could be riding the waves in the dawn and admiring the green carpeted mountains by dusk. Travel destinations in Sri Lanka provide an array of holiday experiences from beach holidays to a marathon of wildlife watching, adrenaline pumping adventure sports and pilgrimage to some of the oldest cities in the world. With so many cultures living next to each other, life in Sri Lanka offers a series of festivities throughout the year.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-header" id="headingTwo">
                                    <p class="panel-topic mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                                        People
                                        </button>
                                    </p>
                                </div>

                                <div id="collapseTwo" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body panel-description">
                                        <p>People Description</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-header" id="headingThree">
                                    <p class="panel-topic mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseOne">
                                        Cultural Heritage
                                        </button>
                                    </p>
                                </div>

                                <div id="collapseThree" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body panel-description">
                                        <p>Cultural Heritage Description</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-header" id="headingFour">
                                    <p class="panel-topic mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseOne">
                                        Festivals Heritage
                                        </button>
                                    </p>
                                </div>

                                <div id="collapseFour" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body panel-description">
                                        <p>Festivals Description</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-header" id="headingFive">
                                    <p class="panel-topic mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseOne">
                                        Spiritual Experiences Heritage
                                        </button>
                                    </p>
                                </div>

                                <div id="collapseFive" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body panel-description">
                                        <p>Spiritual Experiences Description</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-header" id="headingSix">
                                    <p class="panel-topic mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseOne">
                                        Ayurveda Heritage
                                        </button>
                                    </p>
                                </div>

                                <div id="collapseSix" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body panel-description">
                                        <p>Ayurveda Description</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-header" id="headingSeven">
                                    <p class="panel-topic mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseOne">
                                        Nature & Wildlife Heritage
                                        </button>
                                    </p>
                                </div>

                                <div id="collapseSeven" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body panel-description">
                                        <p>Nature & Wildlife Description</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-header" id="headingEight">
                                    <p class="panel-topic mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="true" aria-controls="collapseOne">
                                        Sports & Adventure
                                        </button>
                                    </p>
                                </div>

                                <div id="collapseEight" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body panel-description">
                                        <p>Sports & Adventure Description</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-header" id="headingNine">
                                    <p class="panel-topic mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="true" aria-controls="collapseOne">
                                        Beaches
                                        </button>
                                    </p>
                                </div>

                                <div id="collapseNine" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body panel-description">
                                        <p>Beaches Description</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-header" id="headingTen">
                                    <p class="panel-topic mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="true" aria-controls="collapseOne">
                                        Hotels
                                        </button>
                                    </p>
                                </div>

                                <div id="collapseTen" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="panel-body panel-description">
                                        <p>Hotels Description</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section mt-3 mt-md-5 mb-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Sri Lanka <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/tourism-1.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>All you need to know <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/tourism-2.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>General Information <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-1.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

</body>
</html>
