<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>German Embassy - Home</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0">
        <div class="home-slider">
            <div class="home-main-slider owl-carousel">
                <div class="item">
                    <div class="slider-img" style="background-image: url('assets/images/slider-img-1.jpg')">                        
                    </div>
                    <div class="slide-txt">
                        <p>Extension of Visas for Foreigners in Sri Lanka</p>
                        <span>HIGH-LEVEL OPEN DEBATE OF THE UN SECURITY COUNCIL ON “PEACE OPERATIONS AND HUMAN RIGHTS”, 7th July 2020</span>
                    </div>
                </div>
                <!-- <div class="item">
                    <div class="slider-img" style="background-image: url('assets/images/slider-img-2.jpg')">                        
                    </div>
                </div> -->
            </div>
        </div>
    </div>

    <main class="main-content homepage-main-content pt-0">

        <div class="page-section home-main-article">
            <div class="container-fluid p-0">
                <div class="main-article-bg"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 order-md-0 order-1">
                            <div class="pt-3">
                                <span>Article</span>
                                <h1>Sri Lanka Tourism Operational Guidelines with Health Protocols Version 1</h1>
                                <p>The Embassy of Sri Lanka in Berlin is happy to present to you the Sri Lanka Tourism Operational
                                Guidelines with Health Protocols - Version 1. The guidelines were drafted in consultation with
                                the Ministry of Health, World Health Organization (WHO), and Tourism industry stakeholders and
                                read as follows:</p>
                            </div>
                        </div>
                        <div class="col-md-7 order-md-1 order-0 pl-0 pl-md-3">
                            <div class="d-flex align-items-center h-100">
                                <img alt="Main article image" class="img-fluid" src="assets/images/so-srilanka.jpg" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section main-topic">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <h1>Actual News</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>The Poson Poya Day Message <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/home-news-1.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>The Poson Poya Day Message <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/home-news-2.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>The Poson Poya Day Message <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/home-news-3.jpg">
                                </div>
                            </a>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>

        <div class="page-section main-topic">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mt-0 mt-md-2">
                            <h1>Quick Links</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section quick-links bottom-space">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="quick-link-item mb-4 mb-md-0">
                            <a>
                                <div><span><svg xmlns="http://www.w3.org/2000/svg" width="22.956" height="36.323" viewBox="0 0 22.956 36.323"><g transform="translate(-87.479)"><g transform="translate(87.479 0)"><path d="M104.243,12.573l6.027-6.586a.6.6,0,0,0,.12-.639.639.639,0,0,0-.559-.359H90.473s-.24,0-.24.04V4.311A2,2,0,0,0,91.91,2.2,2.2,2.2,0,0,0,89.754,0a2.275,2.275,0,0,0-2.275,2.2,2.355,2.355,0,0,0,1.557,2.115V35.724a.6.6,0,1,0,1.2,0V20.157h19.6a.559.559,0,0,0,.559-.359.639.639,0,0,0-.12-.639Z" transform="translate(-87.479 0)"/></g></g></svg></span> <p>Visa</p></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="quick-link-item mb-4 mb-md-0">
                            <a>
                                <div><span><svg xmlns="http://www.w3.org/2000/svg" width="22.318" height="35.171" viewBox="0 0 22.318 35.171"><g transform="translate(-99.9 -10)"><path d="M120.567,14.947h-1.923V10.429a.43.43,0,0,0-.429-.429.63.63,0,0,0-.122.014l-16.628,4.933A1.655,1.655,0,0,0,99.9,16.6V43.52a1.652,1.652,0,0,0,1.651,1.651h19.015a1.652,1.652,0,0,0,1.651-1.651V16.6a1.652,1.652,0,0,0-1.651-1.651ZM117.786,11V14.94h-13.3ZM121.36,43.52a.8.8,0,0,1-.794.793H101.551a.8.8,0,0,1-.793-.793V16.6a.792.792,0,0,1,.393-.686l.007.021.436-.129h18.972a.8.8,0,0,1,.794.793Z" transform="translate(0 0)"/><path d="M176.3,377.7H163.929a.429.429,0,0,0,0,.858H176.3a.429.429,0,1,0,0-.858Z" transform="translate(-59.054 -341.415)"/><path d="M216.576,418h-5.647a.429.429,0,0,0,0,.858h5.647a.429.429,0,0,0,0-.858Z" transform="translate(-102.694 -378.834)"/><path d="M172.155,153.6a6.455,6.455,0,1,0,6.455,6.455A6.457,6.457,0,0,0,172.155,153.6Zm-1.959,1.215a6.3,6.3,0,0,0-1.251,1.887,5.506,5.506,0,0,1-.736-.615A5.568,5.568,0,0,1,170.2,154.815Zm-2.545,1.923a6.63,6.63,0,0,0,.994.793,8.666,8.666,0,0,0-.343,2.095h-1.723A5.55,5.55,0,0,1,167.652,156.738Zm-1.072,3.739H168.3a8.666,8.666,0,0,0,.343,2.095,6.207,6.207,0,0,0-.994.793,5.572,5.572,0,0,1-1.072-2.888Zm1.637,3.539a5.5,5.5,0,0,1,.736-.615,6.3,6.3,0,0,0,1.251,1.887A5.541,5.541,0,0,1,168.216,164.016Zm3.51,1.415a5.169,5.169,0,0,1-2.03-2.473,5.47,5.47,0,0,1,2.03-.55Zm0-3.882a6.445,6.445,0,0,0-2.309.593,8.163,8.163,0,0,1-.257-1.666h2.566Zm0-1.93H169.16a8.163,8.163,0,0,1,.257-1.666,6.338,6.338,0,0,0,2.309.593Zm0-1.93a5.572,5.572,0,0,1-2.03-.55,5.231,5.231,0,0,1,2.03-2.466Zm4.375-1.6a5.016,5.016,0,0,1-.736.615,6.33,6.33,0,0,0-1.251-1.894A5.789,5.789,0,0,1,176.1,156.088Zm-3.517-1.415a5.257,5.257,0,0,1,2.037,2.473,5.47,5.47,0,0,1-2.037.55Zm0,3.882a6.373,6.373,0,0,0,2.316-.593,8.171,8.171,0,0,1,.257,1.658h-2.574Zm0,1.923h2.566a7.7,7.7,0,0,1-.257,1.658,6.373,6.373,0,0,0-2.316-.593v-1.065Zm0,4.954v-3.024a5.469,5.469,0,0,1,2.037.55A5.222,5.222,0,0,1,172.584,165.431Zm1.53-.143a6.33,6.33,0,0,0,1.251-1.894,6.1,6.1,0,0,1,.736.615A5.586,5.586,0,0,1,174.114,165.288Zm2.545-1.923a5.906,5.906,0,0,0-.994-.793,8.607,8.607,0,0,0,.343-2.087h1.723A5.572,5.572,0,0,1,176.659,163.365Zm-.651-3.746a8.607,8.607,0,0,0-.343-2.087,6.63,6.63,0,0,0,.994-.793,5.549,5.549,0,0,1,1.072,2.881Z" transform="translate(-61.096 -133.335)"/></g></svg></span> <p>Passport</p></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="quick-link-item mb-4 mb-md-0">
                            <a>
                                <div><span>
                                    <img class="mt-1" alt="A-Z image"  src="assets/images/a-z.png" >
                                </span> <p>A - Z</p></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

<script>
    $('.home-main-slider').owlCarousel({
    items:1,
    loop:false,
    margin:10,
    nav:true,
    autoplay:true,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    // responsive:{
    //     0:{
    //         items:2
    //     },
    //     600:{
    //         items:2
    //     },
    //     1000:{
    //         items:2
    //     }
    // }
})
</script>


</body>
</html>
