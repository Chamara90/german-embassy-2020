<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>German Embassy - Trade and Investment</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0 inner-page-main-topic">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="topic-wrap">
                        <h1>Trade and Investment</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <main class="main-content homepage-main-content pt-0">

        <div class="container">
            <div class="breadcrumb-section">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Trade and Investment</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="page-section mb-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Investment in Sri Lanka <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/home-news-1.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Trading with Sri Lanka <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/about-us-2.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="news-card">                   
                            <a>                            
                                <h2>General Information <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-1.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid p-0 inner-page-secondary-topic">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="topic-wrap">
                            <h1>Trade Agreements</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid p-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="description">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, a ugue velit cursus nunc,</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid p-0 inner-page-secondary-topic mt-4 pt-3">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="topic-wrap">
                            <h1>Commercial Data</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid p-0 mb-5 pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="description">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, a ugue velit cursus nunc,</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

</body>
</html>
