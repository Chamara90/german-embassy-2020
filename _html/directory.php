<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>German Embassy - Directory</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0 inner-page-main-topic">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="topic-wrap">
                        <h1>Directory</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <main class="main-content homepage-main-content pt-0">

        <div class="container">
            <div class="breadcrumb-section">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Directory</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="search-section">
                            <h3>Search Members</h3>
                            <form>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Company Name">
                                </div>
                                <div class="form-group">
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        <option>Core Business Option 1</option>
                                        <option>Core Business Option 2</option>
                                        <option>Core Business Option 3</option>
                                        <option>Core Business Option 4</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="form-control" id="exampleFormControlSelect2">
                                        <option>Service Sectors 1</option>
                                        <option>Service Sectors 2</option>
                                        <option>Service Sectors 3</option>
                                        <option>Service Sectors 4</option>
                                    </select>
                                </div>
                                <div>
                                    <button class="btn btn-primary search-section-btn mr-2" type="">Search</button>
                                    <button class="btn btn-primary search-section-btn" type="">Clear</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section mt-5 mb-2 mb-mb-5 pb-2 pb-md-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="partners-section p-2">
                            <div class="partner">
                                <div class="row">
                                    <div class="col-3 p-3 d-flex align-items-center">
                                        <div class="partner-image">
                                            <img alt="Partner Image" class="img-fluid" src="assets/images/mobitel-logo.jpg">
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="partner-info">
                                            <h4 class="mt-2">Frost Colombo (PVT) Limited</h4>
                                            <p>Core Business Focus : IT/ Software products</p>
                                            <p>Address : Level 1, 208, Stanley Thilakaratne Mawatha, Jubilee Post, Nugegoda</p>
                                            <a class="msg-btn" href="#"><span><i class="fa fa-envelope" aria-hidden="true"></i></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="partners-section p-2">
                            <div class="partner">
                                <div class="row">
                                    <div class="col-3 p-3 d-flex align-items-center">
                                        <div class="partner-image">
                                            <img alt="Partner Image" class="img-fluid" src="assets/images/com-bnk-logo.jpg">
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="partner-info">
                                            <h4 class="mt-2">Frost Colombo (PVT) Limited</h4>
                                            <p>Core Business Focus : IT/ Software products</p>
                                            <p>Address : Level 1, 208, Stanley Thilakaratne Mawatha, Jubilee Post, Nugegoda</p>
                                            <a class="msg-btn" href="#"><span><i class="fa fa-envelope" aria-hidden="true"></i></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="partners-section p-2">
                            <div class="partner">
                                <div class="row">
                                    <div class="col-3 p-3 d-flex align-items-center">
                                        <div class="partner-image">
                                            <img alt="Partner Image" class="img-fluid" src="assets/images/pb-logo.jpg">
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="partner-info">
                                            <h4 class="mt-2">Frost Colombo (PVT) Limited</h4>
                                            <p>Core Business Focus : IT/ Software products</p>
                                            <p>Address : Level 1, 208, Stanley Thilakaratne Mawatha, Jubilee Post, Nugegoda</p>
                                            <a class="msg-btn" href="#"><span><i class="fa fa-envelope" aria-hidden="true"></i></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="partners-section p-2">
                            <div class="partner">
                                <div class="row">
                                    <div class="col-3 p-3 d-flex align-items-center">
                                        <div class="partner-image">
                                            <img alt="Partner Image" class="img-fluid" src="assets/images/jr-designs.jpg">
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="partner-info">
                                            <h4 class="mt-2">Frost Colombo (PVT) Limited</h4>
                                            <p>Core Business Focus : IT/ Software products</p>
                                            <p>Address : Level 1, 208, Stanley Thilakaratne Mawatha, Jubilee Post, Nugegoda</p>
                                            <a class="msg-btn" href="#"><span><i class="fa fa-envelope" aria-hidden="true"></i></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="partners-section p-2">
                            <div class="partner">
                                <div class="row">
                                    <div class="col-3 p-3 d-flex align-items-center">
                                        <div class="partner-image">
                                            <img alt="Partner Image" class="img-fluid" src="assets/images/polaris-trade.jpg">
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="partner-info">
                                            <h4 class="mt-2">Frost Colombo (PVT) Limited</h4>
                                            <p>Core Business Focus : IT/ Software products</p>
                                            <p>Address : Level 1, 208, Stanley Thilakaratne Mawatha, Jubilee Post, Nugegoda</p>
                                            <a class="msg-btn" href="#"><span><i class="fa fa-envelope" aria-hidden="true"></i></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="partners-section p-2">
                            <div class="partner">
                                <div class="row">
                                    <div class="col-3 p-3 d-flex align-items-center">
                                        <div class="partner-image">
                                            <img alt="Partner Image" class="img-fluid" src="assets/images/kp-kapital.jpg">
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="partner-info">
                                            <h4 class="mt-2">Frost Colombo (PVT) Limited</h4>
                                            <p>Core Business Focus : IT/ Software products</p>
                                            <p>Address : Level 1, 208, Stanley Thilakaratne Mawatha, Jubilee Post, Nugegoda</p>
                                            <a class="msg-btn" href="#"><span><i class="fa fa-envelope" aria-hidden="true"></i></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="partners-section p-2">
                            <div class="partner">
                                <div class="row">
                                    <div class="col-3 p-3 d-flex align-items-center">
                                        <div class="partner-image">
                                            <img alt="Partner Image" class="img-fluid" src="assets/images/corporate-logo.jpg">
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="partner-info">
                                            <h4 class="mt-2">Frost Colombo (PVT) Limited</h4>
                                            <p>Core Business Focus : IT/ Software products</p>
                                            <p>Address : Level 1, 208, Stanley Thilakaratne Mawatha, Jubilee Post, Nugegoda</p>
                                            <a class="msg-btn" href="#"><span><i class="fa fa-envelope" aria-hidden="true"></i></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="partners-section p-2">
                            <div class="partner">
                                <div class="row">
                                    <div class="col-3 p-3 d-flex align-items-center">
                                        <div class="partner-image">
                                            <img alt="Partner Image" class="img-fluid" src="assets/images/huawei-logo.jpg">
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="partner-info">
                                            <h4 class="mt-2">Frost Colombo (PVT) Limited</h4>
                                            <p>Core Business Focus : IT/ Software products</p>
                                            <p>Address : Level 1, 208, Stanley Thilakaratne Mawatha, Jubilee Post, Nugegoda</p>
                                            <a class="msg-btn" href="#"><span><i class="fa fa-envelope" aria-hidden="true"></i></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- <div class="page-section mt-5 mb-5 pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="directory-pagination">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

</body>
</html>
