<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>German Embassy - Education</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0 inner-page-main-topic">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="topic-wrap">
                        <h1>Education</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <main class="main-content homepage-main-content pt-0">

        <div class="container">
            <div class="breadcrumb-section">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Education</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="page-section mb-3 mb-md-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Studying in Germany and Switzerland <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/edu-1.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Studying and Internships in Sri Lanka <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/edu-2.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                   
                            <a>                            
                                <h2>General Information <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-1.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

</body>
</html>
