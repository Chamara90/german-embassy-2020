<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Lipton Reach</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0">
        <div class="inner-page-cover">
            <div class="inner-page-cover-img" style="background-image: url('assets/images/contact-cover.jpg')">
            
            </div>
        </div>
    </div>

    <main id="elements-page" class="main-content homepage-main-content pt-0">


        <div class="page-section contact-page pt-5 pb-4">            
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="inner-topic">
                            <h3>Lipton Reach Bungalow</h3>
                        </div>
                        <ul class="contact-data-list">
                            <li>
                                <span>
                                    <svg id="phone_1_" data-name="phone (1)" xmlns="http://www.w3.org/2000/svg" width="49.295" height="49.295" viewBox="0 0 49.295 49.295">
                                    <g id="Group_7" data-name="Group 7">
                                        <path id="Path_4" data-name="Path 4" d="M45.34,32.357a27.988,27.988,0,0,1-8.79-1.4,4.018,4.018,0,0,0-3.907.824L27.1,35.965A30.631,30.631,0,0,1,13.328,22.2l4.061-5.4a3.984,3.984,0,0,0,.98-4.037,28.031,28.031,0,0,1-1.405-8.8A3.96,3.96,0,0,0,13.008,0H3.956A3.96,3.96,0,0,0,0,3.956,45.391,45.391,0,0,0,45.34,49.3,3.96,3.96,0,0,0,49.3,45.34V36.312A3.96,3.96,0,0,0,45.34,32.357Z" fill="#fff"/>
                                    </g>
                                    </svg>
                                </span>
                                <p>+94 777 386686</p>
                            </li>
                            <li>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="49.999" height="48.221" viewBox="0 0 49.999 48.221">
                                    <g id="telephone" transform="translate(0 -9.1)">
                                        <g id="Group_2" data-name="Group 2" transform="translate(0 9.1)">
                                        <g id="Group_1" data-name="Group 1" transform="translate(0 0)">
                                            <path id="Path_1" data-name="Path 1" d="M48.083,18.421c-1.919-2.64-5.278-5.023-9.46-6.709a37.015,37.015,0,0,0-27.211-.041c-4.189,1.674-7.557,4.046-9.483,6.68A8.578,8.578,0,0,0,.388,26.235,4.132,4.132,0,0,0,4.348,29.09l6.245.006H10.6a4.172,4.172,0,0,0,3.21-1.5,4.1,4.1,0,0,0,.894-3.374q-.01-.053-.023-.105l-.329-1.278A25.479,25.479,0,0,1,25,20.324a26.156,26.156,0,0,1,10.647,2.569l-.334,1.27q-.014.054-.024.109a4.1,4.1,0,0,0,.884,3.376,4.172,4.172,0,0,0,3.213,1.506l6.245.006h.006A4.132,4.132,0,0,0,49.6,26.316,8.586,8.586,0,0,0,48.083,18.421Z" transform="translate(0 -9.1)" fill="#fff"/>
                                        </g>
                                        </g>
                                        <g id="Group_4" data-name="Group 4" transform="translate(5.218 26.657)">
                                        <g id="Group_3" data-name="Group 3">
                                            <path id="Path_2" data-name="Path 2" d="M87.334,197.768l-4.657-4.89a1.492,1.492,0,0,0-1.08-.463h-3.01v-2.034a1.492,1.492,0,1,0-2.984,0v2.034H70.825v-2.034a1.492,1.492,0,0,0-2.984,0v2.034h-3.01a1.492,1.492,0,0,0-1.08.463l-4.657,4.89a20.44,20.44,0,0,0-5.662,14.156v6.138a1.492,1.492,0,0,0,1.492,1.492H91.5A1.492,1.492,0,0,0,93,218.062v-6.138A20.44,20.44,0,0,0,87.334,197.768ZM73.214,214.1a7.808,7.808,0,1,1,7.808-7.808A7.817,7.817,0,0,1,73.214,214.1Z" transform="translate(-53.432 -188.89)" fill="#fff"/>
                                        </g>
                                        </g>
                                        <g id="Group_6" data-name="Group 6" transform="translate(20.176 39.235)">
                                        <g id="Group_5" data-name="Group 5" transform="translate(0 0)">
                                            <path id="Path_3" data-name="Path 3" d="M211.427,317.691a4.824,4.824,0,1,0,4.824,4.824A4.83,4.83,0,0,0,211.427,317.691Z" transform="translate(-206.603 -317.691)" fill="#fff"/>
                                        </g>
                                        </g>
                                    </g>
                                    </svg>
                                </span>
                                <p>+94 112 575661</p>
                            </li>
                            <li>
                                <span><svg xmlns="http://www.w3.org/2000/svg" width="37.02" height="48.591" viewBox="0 0 37.02 48.591">
                                <g id="pin" transform="translate(-60.962 0)">
                                    <g id="Group_11" data-name="Group 11" transform="translate(60.962 0)">
                                    <g id="Group_10" data-name="Group 10">
                                        <path id="Path_5" data-name="Path 5" d="M97.082,12.631A18.035,18.035,0,0,0,85.338.886,18.888,18.888,0,0,0,68.5,3.584a18.6,18.6,0,0,0-7.537,14.909,18.351,18.351,0,0,0,3.695,11.1l14.817,19,14.817-19A18.712,18.712,0,0,0,97.082,12.631ZM79.474,28.46a9.968,9.968,0,1,1,9.968-9.968A9.98,9.98,0,0,1,79.474,28.46Z" transform="translate(-60.962 0)" fill="#fff"/>
                                    </g>
                                    </g>
                                    <g id="Group_13" data-name="Group 13" transform="translate(72.354 11.391)">
                                    <g id="Group_12" data-name="Group 12" transform="translate(0)">
                                        <path id="Path_6" data-name="Path 6" d="M188.118,120.027a7.11,7.11,0,1,0,7.12,7.1A7.112,7.112,0,0,0,188.118,120.027Z" transform="translate(-180.998 -120.027)" fill="#fff"/>
                                    </g>
                                    </g>
                                </g>
                                </svg>
                                </span>
                                <p>No. 48, Kebillewela South, Bandarawela, Srilanka</p>
                            </li>
                            <li>
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="38.085" viewBox="0 0 50 38.085">
                                        <g id="mail_1_" data-name="mail (1)" transform="translate(0 -61)">
                                            <g id="Group_26" data-name="Group 26" transform="translate(2.512 61)">
                                            <g id="Group_25" data-name="Group 25" transform="translate(0 0)">
                                                <path id="Path_7" data-name="Path 7" d="M68.814,61H27.6a4.343,4.343,0,0,0-1.883.44l22.39,22.39,5.018-4.823h0L70.7,61.441A4.344,4.344,0,0,0,68.814,61Z" transform="translate(-25.721 -61)" fill="#fff"/>
                                            </g>
                                            </g>
                                            <g id="Group_28" data-name="Group 28" transform="translate(33.028 63.512)">
                                            <g id="Group_27" data-name="Group 27" transform="translate(0 0)">
                                                <path id="Path_8" data-name="Path 8" d="M354.744,86.728l-16.531,16.53,16.531,16.531a4.343,4.343,0,0,0,.441-1.883V88.61A4.342,4.342,0,0,0,354.744,86.728Z" transform="translate(-338.213 -86.728)" fill="#fff"/>
                                            </g>
                                            </g>
                                            <g id="Group_30" data-name="Group 30" transform="translate(0 63.512)">
                                            <g id="Group_29" data-name="Group 29" transform="translate(0 0)">
                                                <path id="Path_9" data-name="Path 9" d="M.44,86.721A4.343,4.343,0,0,0,0,88.6v29.3a4.344,4.344,0,0,0,.44,1.882l16.531-16.53Z" transform="translate(0 -86.721)" fill="#fff"/>
                                            </g>
                                            </g>
                                            <g id="Group_32" data-name="Group 32" transform="translate(2.511 82.114)">
                                            <g id="Group_31" data-name="Group 31">
                                                <path id="Path_10" data-name="Path 10" d="M54.16,277.211l-5.019,4.823a1.464,1.464,0,0,1-2.071,0l-4.824-4.824L25.714,293.742a4.343,4.343,0,0,0,1.883.441h41.21a4.344,4.344,0,0,0,1.883-.44Z" transform="translate(-25.714 -277.211)" fill="#fff"/>
                                            </g>
                                            </g>
                                        </g>
                                        </svg>
                                    </span>
                                    <p><a href="mailto:liptonreach@gmail.com">liptonreach@gmail.com</a></p></li>
                        </lu>

                        <!-- <br>
                        <br>
                        <br>
                        <ul class="contact-data-list">
                            <li><span><i class="fa fa-phone" aria-hidden="true"></i> </span><p>+94 777 386686</p></li>
                            <li><span><i class="fa fa-phone" aria-hidden="true"></i> </span><p>+94 112 575661</p></li>
                            <li><span><i class="fa fa-map-marker" aria-hidden="true"></i> </span><p>No. 48, Kebillewela South, Bandarawela, Srilanka</p></li>
                            <li><span><i class="fa fa-envelope" aria-hidden="true"></i> </span><p><a href="mailto:liptonreach@gmail.com">liptonreach@gmail.com</a></p></li>
                        </lu> -->

                        <div>
                            <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9422.064100951622!2d80.99866262232253!3d6.831704323433343!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae46ff6b1a9c4f5%3A0x812c17fb9e76778!2sDhamsen%20Hotel!5e0!3m2!1sen!2slk!4v1598248402335!5m2!1sen!2slk" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4711.047089958395!2d81.00165727556023!3d6.8301774032150435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae46ff128fee649%3A0x4f7494f44ca2402d!2sKinigama%20Rd%2C%20Bandarawela!5e0!3m2!1sen!2slk!4v1598273246177!5m2!1sen!2slk" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="contact-form-container">
                            <form id="contact-form" class="contact-form">
                                <div class="form-group input-filed">
                                    <p>Name<span>*</span></p>
                                    <input type="text" class="form-control" name="userName" id="name" aria-describedby="" placeholder="Name*">                                   
                                </div>

                                <div class="form-group input-filed">
                                    <p>Email<span>*</span></p>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="E-mail*">
                                </div>

                                <div class="form-group input-filed">
                                    <p>Country</p>
                                    <input type="text" class="form-control" name="country" id="country" aria-describedby="" placeholder="Country">                                   
                                </div>

                                <div class="form-group input-filed">
                                    <p>Phone</p>
                                    <input type="text" class="form-control" name="phone" id="phone" aria-describedby="" placeholder="Phone">                                   
                                </div>

                                <div class="form-group input-filed">
                                    <p>Date of Arrival</p>
                                    <input type="text" class="form-control" name="arrivaldate" id="arrivaldate" aria-describedby="" placeholder="Arrival Date">                                   
                                </div>

                                <div class="form-group input-filed">
                                    <p>Number of Nights</p>
                                    <input type="text" class="form-control" name="nightcount" id="nightcount" aria-describedby="" placeholder="Number of Nights">                                   
                                </div>

                                <div class="form-group input-filed">
                                    <p>Adults</p>
                                    <input type="text" class="form-control" name="adults" id="adults" aria-describedby="" placeholder="Adults">                                   
                                </div>

                                <div class="form-group input-filed">
                                    <p>Children</p>
                                    <input type="text" class="form-control" name="children" id="children" aria-describedby="" placeholder="Children">                                   
                                </div>

                                <div class="form-group input-filed">
                                    <p>Accommodation</p>
                                    <div class="checkbox-group">

                                        <div class="form-check">
                                            <label class="form-check-label" for="radio1">
                                                <input type="radio" class="rd-btn form-check-input" id="radio1" name="optradio" value="option1" checked><span>Whole Bungalow</span>
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <label class="form-check-label" for="radio2">
                                                <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2"><span>Single Room</span>
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <label class="form-check-label" for="radio3">
                                                <input type="radio" class="form-check-input" id="radio3" name="optradio" value="option3"><span>Two Rooms</span>
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <label class="form-check-label" for="radio4">
                                                <input type="radio" class="form-check-input" id="radio4" name="optradio" value="option4"><span>Three Rooms</span>
                                            </label>
                                        </div>

                                        <div class="form-check">
                                            <label class="form-check-label" for="radio5">
                                                <input type="radio" class="form-check-input" id="radio5" name="optradio" value="option5"><span>Four Rooms</span>
                                            </label>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group input-filed">
                                    <p for="exampleFormControlTextarea1">Message</p>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="10" placeholder="Message"></textarea>
                                </div>
                                <div class="submit-btn-container">
                                    <button type="submit" class="btn btn-primary br-btn">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        

        

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>
<script>
$(function () {
  $("#datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true
  }).datepicker('update', new Date());
});

</script>

</body>
</html>
