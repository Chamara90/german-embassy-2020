<footer class="footer-section pb-3" id="footer">
    <div class="container-fluid p-0">
      <div class="container">
          <div class="footer-social-ico">            
                <div class="row">
                    <div class="col-4">
                      <div class="social-ico text-left">
                        <a href="#"><span class="d-flex align-items-center justify-content-center">
                          <i class="fa fa-twitter" aria-hidden="true"></i>
                        </span></a>
                      </div>
                    </div>
                    <div class="col-4 text-center">
                      <div class="social-ico">
                        <a href="#"><span class="d-flex align-items-center justify-content-center">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                        </span></a>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="social-ico float-right">
                        <a href="#"><span class="d-flex align-items-center justify-content-center">
                        <i class="fa fa-instagram" aria-hidden="true"></i>
                        </span></a>
                      </div>
                    </div>
                </div>
            </div>
        </div>   
        <div class="container">
            <div class="row">
              <div class="col-md-3">
                  <div class="footer-links">
                    <p class="footer-header">Quick Links</p>
                    <ul>
                      <li><a>Home <span>&#8594;</span></a></li>
                      <li><a>About Us <span>&#8594;</span></a></li>
                      <li><a>Consular <span>&#8594;</span></a></li>
                      <li><a>Tourism <span>&#8594;</span></a></li>
                      <li><a>Trade and Investment <span>&#8594;</span></a></li>
                      <li><a>Education <span>&#8594;</span></a></li>
                    </ul>
                  </div>
              </div>
              <div class="col-md-3">
                  <div class="footer-links">
                    <p class="footer-header">Services</p>
                    <ul>
                      <li><a>Disclaimer <span>&#8594;</span></a></li>
                    </ul>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="footer-links">
                    <p class="footer-header">Contacts</p>
                    <p class="address">Embassy of Sri Lanka,<br>
                       Niklasstraße 19<br>
                       14163 Berlin.<br>
                       Tel. (030) 80909749<br>
                       Fax (030) 80909757<br>
                       E-Mail:slemb.berlin@mfa.gov.lk<br>
                       Connections: S1 Mexikoplatz; Busse 118, <br>
                       629 Niklasstraße</p>
                  </div>
              </div>
            </div>
        </div>
    </div>
    <a id="back-to-top" href="#" class="back-to-top" role="button">Top of Page<i class="fa fa-angle-up ml-1" aria-hidden="true"></i></a>
</footer>
