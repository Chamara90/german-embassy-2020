<header id="header">
  
  <div class="container-fluid top-info pl-0 pr-0 pl-md-3 pr-md-3 pt-2 pt-md-0">
    <div class="container">
      <div class="row">
        <div class="col-10 col-md-8">
          <div class="logo-container text-left">
            <a href="index.php"><img alt="Logo" class="main-logo img-fluid" src="assets/images/logo.png"></a>
          </div>
        </div>
        <div class="col-2 col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
          <div>
          <div class="top-bar-search-slot">
            <div class="d-none justify-content-between mb-1 d-none d-md-flex">
                <div>
                  <p>Last update:Wed, 15 Jul 2020</p>
                </div>
          
                <div>
                  <div class="dropdown show">
                    <a class="btn btn-sm dropdown-toggle dropdown-btn" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      English
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                      <a class="dropdown-item" href="#">Sinhala</a>
                      <a class="dropdown-item" href="#">German</a>
                    </div>
                  </div>
                </div> 
              </div>
              <div class="d-flex justify-content-start">
                <form class="search-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search" name="">
                        <div class="input-group-btn">
                            <button class="btn" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </form>
                <div class="header-fb d-none d-md-block">
                  <a><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                </div>
              </div> 
          </div class="d-flex justify-content-between">
            <span class="d-inline-block d-md-none mobile-search-icon float-left"><i class="fa fa-search" aria-hidden="true"></i></span>
            <button class="navbar-toggler p-0 d-block d-md-none float-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
            </button>
            </div>
        </div>

        <div class="col-md-12">
          <nav class="navbar navbar-expand-md p-0 pt-3">
            <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
            </button> -->
            <div class="collapse navbar-collapse pb-3 pb-md-0" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                  <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="about-us.php">About Us</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="consulate-general-sri-lanka.php">Consulate General of Sri Lanka</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="tourism.php">Tourism</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="trade-and-investment.php">Trade and Investment</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="education.php">Education</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="directory.php">Directory</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="news-events.php">News and Events</a>
                </li>
              </ul>
            </div>
          </nav>
        </div>  

      </div>
    </div>
  </div>

  <!-- <div class="container">
    <nav class="navbar navbar-expand-md p-0 pt-3">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about-us.php">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="consulate-general-sri-lanka.php">Consulate General of Sri Lanka</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="tourism.php">Tourism</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="trade-and-investment.php">Trade and Investment</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="education.php">Education</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="directory.php">Directory</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="news-events.php">News and Events</a>
          </li>
        </ul>
      </div>
    </nav>

  </div> -->
  
</header>
