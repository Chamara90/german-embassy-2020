<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>German Embassy - About Us</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0 inner-page-main-topic">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="topic-wrap">
                        <h1>ABOUT US</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <main class="main-content homepage-main-content pt-0">

        <div class="container">
            <div class="breadcrumb-section">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">About Us</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Mission Holidays <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/home-news-1.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Organization Chart <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/about-us-2.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                   
                            <a>                            
                                <h2>Consulates <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/about-us-3.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Accreditation <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/about-us-4.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>History of Sri Lanka <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/about-us-5.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>History of Embassy <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/about-us-6.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>                            
                                <h2>History of Relations with Germany, Switzerland, Croatia, North Macedonia and Montenego <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/about-us-7.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

</body>
</html>
