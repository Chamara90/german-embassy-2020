<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>German Embassy - About Us</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0 inner-page-main-topic">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="topic-wrap">
                        <h1>Consulate General of Sri Lanka</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <main class="main-content homepage-main-content pt-0">

        <div class="container">
            <div class="breadcrumb-section">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Consulate General of Sri Lanka</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="container-fluid p-0 inner-page-secondary-topic">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="topic-wrap">
                            <h1>Citizenship Services</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <div class="page-section mb-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>General Information <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-1.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Dual Citizenships <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-2.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>News Passports <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-3.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Renewals / Endorsements <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-4.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Birth Registration <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-5.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Death Registration <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-6.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Marriage <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-7.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid p-0 inner-page-secondary-topic">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="topic-wrap">
                            <h1>Visa</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section mb-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>General Information <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-8.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Tourist Visa <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-9.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Residence Visa <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-10.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Work Visa <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-11.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Student Visa <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-12.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Journalist Visa <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>My Dream Home Programme <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-13.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>                            
                                <h2>Important Information <span>&#8594;</span></h2>
                                <div class="news-image">
                                    <img alt="News image" class="img-fluid" src="assets/images/con-14.jpg">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid p-0 notice mb-5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="notice-wrap">
                            <h2>Please note:</h2>
                            <p>1. The Embassy and the Consulate General operate a baggage search scheme. To avoid delays you are asked to refrain from bringing any baggage, briefcases, laptops etc. with you.</p>
                            <p>2. Mobile phones and any other electronic equipment must be switched off before entry and remain so during your stay.</p>
                            <p>3. Photography on the premises is strictly forbidden.</p>
                            <p>4. Entry is at the discretion of the Embassy or Consulate General security staff. Please look after your personal belongings as the Embassy and the Consulate General cannot accept liability for loss or theft.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid p-0 inner-page-secondary-topic">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="topic-wrap">
                            <h1>Downloads, Fees and Bank Details</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section mb-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="news-card">                      
                            <a>       
                                <h2>Forum <span>&#8594;</span></h2>                     
                                <div class="link-card d-flex align-items-center justify-content-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="117.832" height="92.995" viewBox="0 0 117.832 92.995"><defs><style>.a{fill:#b0b3b8;}</style></defs><g transform="translate(0 -3.24)"><path class="a" d="M84.793,9.67H81.566V45.143c0,5.523-3.216,10.517-9.662,10.517H8.743v1.686c0,4.883,5.554,9.812,11.257,9.812H68.289L86.764,78.02,84.084,67.158h.709c5.7,0,8.271-4.917,8.271-9.812V18.091C93.065,13.208,90.493,9.67,84.793,9.67Z" transform="translate(24.767 18.215)"/><path class="a" d="M87.242,3.24H13.882C7.428,3.24,0,8.974,0,14.5V58.961c0,5.09,6.293,8.766,12.33,9.333L8.4,83.208,33.575,68.4H87.242c6.454,0,12.411-3.913,12.411-9.436V14.5C99.653,8.974,93.693,3.24,87.242,3.24ZM25.074,40.771A6.627,6.627,0,1,1,31.7,34.144,6.626,6.626,0,0,1,25.074,40.771Zm24.752,0a6.627,6.627,0,1,1,6.627-6.627A6.626,6.626,0,0,1,49.826,40.771Zm24.756,0a6.627,6.627,0,1,1,6.627-6.627A6.63,6.63,0,0,1,74.583,40.771Z" transform="translate(0 0)"/></g></svg>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>       
                                <h2>Downloads <span>&#8594;</span></h2>                     
                                <div class="link-card d-flex align-items-center justify-content-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="105.952" height="105.952" viewBox="0 0 105.952 105.952"><defs><style>.a{fill:#b0b3b8;}</style></defs><path class="a" d="M100.174,5.778A19.6,19.6,0,0,0,86.224,0h-66.5A19.751,19.751,0,0,0,0,19.728v66.5a19.751,19.751,0,0,0,19.728,19.728h66.5a19.751,19.751,0,0,0,19.728-19.728v-66.5A19.6,19.6,0,0,0,100.174,5.778ZM34.324,45.008a3.1,3.1,0,0,1,4.39,0L49.872,56.166V23.053a3.1,3.1,0,1,1,6.208,0V56.165L67.238,45.007a3.1,3.1,0,0,1,4.39,4.39L55.171,65.854a3.1,3.1,0,0,1-4.39,0L34.324,49.4A3.1,3.1,0,0,1,34.324,45.008ZM82.9,86H23.053a3.1,3.1,0,1,1,0-6.208H82.9A3.1,3.1,0,0,1,82.9,86Z"/></svg>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 pl-md-3 pr-md-3">
                        <div class="news-card">                      
                            <a>       
                                <h2>Fees and Bank Details <span>&#8594;</span></h2>                     
                                <div class="link-card d-flex align-items-center justify-content-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="116.523" height="116.28" viewBox="0 0 116.523 116.28"><defs><style>.a{fill:#b0b3b8;}</style></defs><path class="a" d="M108.018,87.9a1.941,1.941,0,0,1,1.95,1.942V107.07a9.71,9.71,0,0,1-9.71,9.71H9.71A9.71,9.71,0,0,1,0,107.07V34.729a9.71,9.71,0,0,1,9.71-9.71h90.548a9.71,9.71,0,0,1,9.71,9.71V51.964a1.941,1.941,0,0,1-1.948,1.942c-2.02-.008-6.641,0-17.955,0A11.654,11.654,0,0,0,78.41,65.558V76.24A11.653,11.653,0,0,0,90.063,87.892C101.546,87.892,106.04,87.908,108.018,87.9Zm8.5-22.34V76.24a7.777,7.777,0,0,1-7.763,7.768h-18.7a7.777,7.777,0,0,1-7.768-7.768V65.558a7.777,7.777,0,0,1,7.768-7.768h18.692A7.777,7.777,0,0,1,116.523,65.558ZM97.345,70.9A1.942,1.942,0,0,0,95.4,68.957H92.49a1.942,1.942,0,0,0,0,3.884H95.4A1.942,1.942,0,0,0,97.345,70.9ZM92.393,12.873l-1.81-.533A1.945,1.945,0,0,0,88.506,13a11.421,11.421,0,0,0-2.787,6.029,1.943,1.943,0,0,0,1.937,2.1h8.291a1.935,1.935,0,0,0,1.912-2.287,7.767,7.767,0,0,0-5.467-5.974Zm-10.58,6.374a12.77,12.77,0,0,1,2.037-6.35,1.939,1.939,0,0,0-1.111-2.867L70.424,6.4a1.912,1.912,0,0,0-2.452,1.864V19.192a1.942,1.942,0,0,0,1.942,1.942h9.959A1.943,1.943,0,0,0,81.812,19.248ZM33.986,21.134h2.67A1.942,1.942,0,0,0,38.6,19.192V2.442A1.942,1.942,0,0,0,36.656.5h-2.67a1.942,1.942,0,0,0-1.942,1.942v16.75A1.942,1.942,0,0,0,33.986,21.134ZM56.319.5h-11.9a1.942,1.942,0,0,0-1.942,1.942v16.75a1.942,1.942,0,0,0,1.942,1.942H62.145a1.942,1.942,0,0,0,1.942-1.942V8.268A7.777,7.777,0,0,0,56.319.5Zm-30.1,0h-2.67a7.768,7.768,0,0,0-7.768,7.768V19.192a1.942,1.942,0,0,0,1.942,1.942h8.5a1.942,1.942,0,0,0,1.942-1.942V2.442A1.942,1.942,0,0,0,26.218.5Z" transform="translate(0 -0.5)"/></svg>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid p-0 info mb-5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="info-wrap">
                            <ul>
                                <li>
                                    <div>
                                        <p>Sri Lanka Consulate General, Frankfurt</p>
                                        <br>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <span>Name</span>
                                        <p>Sri Lanka Consulate General, Frankfurt</p>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <span>Head</span>
                                        <p>Barbara Quick, Consul General</p>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <span>Tel</span>
                                        <p>+44 131 337 2323</p>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <span>Fax</span>
                                        <p>+44 131 3479 873</p>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <p>Open site plan <span><svg xmlns="http://www.w3.org/2000/svg" width="22.828" height="25.973" viewBox="0 0 22.828 25.973"><g transform="translate(-31)"><g transform="translate(34.044)"><path d="M99.37,0a8.366,8.366,0,0,0-6.8,13.241l6.163,9.617a.761.761,0,0,0,1.281,0l6.19-9.65A8.37,8.37,0,0,0,99.37,0Zm0,12.175a3.8,3.8,0,1,1,3.8-3.8A3.809,3.809,0,0,1,99.37,12.175Z" transform="translate(-91)"/></g><g transform="translate(31 17.486)"><path d="M48.363,344.7l-3.832,5.99a2.516,2.516,0,0,1-4.235,0l-3.838-5.99c-3.377.781-5.458,2.211-5.458,3.92,0,2.966,5.881,4.566,11.414,4.566s11.414-1.6,11.414-4.566C53.828,346.906,51.744,345.475,48.363,344.7Z" transform="translate(-31 -344.695)"/></g></g></svg></span></p>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <p>Further information <span>&#8594;</span> in our data protection declaration</p>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <span>Postal address</span>
                                        <p>16 Eglinton Crescent<br>
                                           Edinburgh, EH12 5DG<br>
                                           United Kingdom</p>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <span>Administrative / consular district</span>
                                        <p>Scotland and the following counties in northern England: Cumbria, Darlington, Durham, Hartlepool, Middlesbrough, Northumberland, Redcar and Cleveland, Stockton on Tees, Tyne and Wear (Tyneside), as well as North Yorkshire with the exception of Selby and York.</p>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <span>Website</span>
                                        <p><a href="https://www.srilanka-botschaft.de/" target="_blank">https://www.srilanka-botschaft.de/</a></p>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <span>E-mail</span>
                                        <p><a href="mailto:E-mail">info@edinburgh.diplo.de</a></p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

</body>
</html>
